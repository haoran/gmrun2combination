``` bash
find /home/hrzhao/Projects/ANA-HDBS-2023-19/run2combination/Fitting/configs/Results_WZ_systematics/ -name "Anal_Run2WZ_*.config" -print0 | xargs -0 -I {} python3 /home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/Scripts/genConfig.py {} /home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02052024/Benchmark/Results_WZ_systematics/

find /home/hrzhao/Projects/ANA-HDBS-2023-19/run2combination/Fitting/configs/Results_ssWW_systematics/ -name "Anal_Run2*_*.config" -print0 | xargs -0 -I {} python3 /home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/Scripts/genConfig.py {} /home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02052024/Benchmark/Results_ssWW_systematics/

find /home/hrzhao/Projects/ANA-HDBS-2023-19/run2combination/Fitting/configs/Results_MultiFit_systematics -name "Anal_Run2*_m*.config" -print0 | xargs -0 -I {} python3 /home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/Scripts/genConfig.py {} /home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02052024/Benchmark/Results_MultiFit_systematics/

find /home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02052024/Benchmark/Results_MultiFit_systematics -type f -name "*.config" -print0 | xargs -0 sed -i 's|config/run2combination/Anal_Run2ssWW|Results_ssWW_systematics/Anal_Run2ssWW|'

find /home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02052024/Benchmark/Results_MultiFit_systematics -type f -name "*.config" -print0 | xargs -0 sed -i 's|config/run2combination/Anal_Run2WZ|Results_WZ_systematics/Anal_Run2WZ|'

```

## Condor

``` bash 

export CONFIG_DIR=/home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02142024/Benchmark_limitsGM
export OUTPUT_DIR=/data/hrzhao/MyOutputs/Run2GMCombination/MyCheckComb/02142024/Benchmark_limitsGM

condor_submit submit_comb_benchmark_variants.condor
# export CONFIG_DIR=/home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02052024/Benchmark/
# export OUTPUT_DIR=/data/hrzhao/MyOutputs/Run2GMCombination/MyCheckComb/02052024/Benchmark/

```