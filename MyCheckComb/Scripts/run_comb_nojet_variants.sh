#!/bin/bash

# Use "$@" to handle all arguments including spaces correctly
wp_mass=$1
project_dir=${2:-"$CONFIG_DIR"}
output_path=${3:-"$OUTPUT_DIR"}
additional_options=Exclude='*JET_*'

if [ -z "$project_dir" ] || [ -z "$output_path" ]; then
    echo "Error: 'project_dir' or 'output_path' is not set."
    exit 1
fi

# Setup environment variables
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir="$HOME/localConfig"
source "${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" 
asetup StatAnalysis, 0.2.5

# Navigate to the project directory
[ -d "$output_path" ] || mkdir -p "$output_path"
cd "$output_path" || exit

base_name=$(basename "$project_dir")
limit_variant="${base_name#Benchmark_}"
# echo $limit_variant

# Array of config files
config_file_patterns=(
  "${project_dir}/Results_WZ_${limit_variant}/Anal_Run2WZ_m${wp_mass}*.config"
  "${project_dir}/Results_ssWW_${limit_variant}/Anal_Run2ssWW_m${wp_mass}*.config"
)

for pattern in "${config_file_patterns[@]}"; do
    # 提取目录路径和文件模式
    dir_path=$(dirname "$pattern")
    file_pattern=$(basename "$pattern")

    # 使用 find 命令搜索匹配的文件，并读取结果到数组
    while IFS= read -r -d $'\0' file; do
        config_files+=("$file")
    done < <(find "$dir_path" -type f -name "$file_pattern" -print0)
done

# for file in "${config_files[@]}"; do
#     echo "Found config file: $file"
# done

# Loop through each config file
for file in "${config_files[@]}"; do
    echo "Running trexfitter for file: $file"
    filename=$(basename "$file")
    log_output_path="${output_path}/logs/log_${filename}"
    mkdir -p "$log_output_path"

    # Array of options for trex-fitter
    options=(h w d b f p l s r i)
    # options=(h w f l)

    # Loop through each option
    for option in "${options[@]}"; do
        # Execute trex-fitter and tee the output to a log file
        trex-fitter "$option" "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_${option}.txt"
    done
done

# Run the combination
file="${project_dir}/Results_MultiFit_${limit_variant}/Anal_Run2Combination_m${wp_mass}.config"
echo "Running trexfitter for file: $file"
filename=$(basename "$file")
log_output_path="${output_path}/logs/log_${filename}"
mkdir -p "$log_output_path"

# trex-fitter mwflr "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_mwflr.txt"
# Array of options for trex-fitter
options=(mw mf ml mr)
for option in "${options[@]}"; do
    # Execute trex-fitter and tee the output to a log file
    trex-fitter "$option" "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_${option}.txt"
done 
