#!/bin/bash

# This script is used to plot the results of the WZ, ssWW and MultiFit 

# The script is called with the following arguments:
# 1. The path to the directory containing the results of the WZ, ssWW and MultiFit
# 2. The path to the directory where the plots will be saved, the default is a plots directory 
#    in the directory containing the results

input_dir=${1:-"/data/hrzhao/MyOutputs/Run2GMCombination/MyCheckComb/02192024/Task1Case1/Benchmark_limitsGM"}
output_dir=${2:-"${input_dir}/Plots/"}

echo "input_dir: $input_dir"
echo "output_dir: $output_dir"

# Create the output directory if it does not exist
mkdir -p $output_dir

cd $output_dir || exit

# Plot the results of the WZ, ssWW and MultiFit
for do_sin in 0 1;
do
    root -b -q "/home/hrzhao/Projects/ANA-HDBS-2023-19/run2combination/Plotting/plot_exclusionlimits/plot_WZlimits.C(\"${input_dir}\", ${do_sin})"
    root -b -q "/home/hrzhao/Projects/ANA-HDBS-2023-19/run2combination/Plotting/plot_exclusionlimits/plot_ssWWlimits.C(\"${input_dir}\", ${do_sin})"
    echo 
done

root -b -q "/home/hrzhao/Projects/ANA-HDBS-2023-19/run2combination/Plotting/plot_exclusionlimits/plot_Combinationlimits.C(\"${input_dir}\", 1, 1, \"./\")"