#!/bin/bash

# Use "$@" to handle all arguments including spaces correctly
wp_mass=$1
project_dir=${2:-"$CONFIG_DIR"}
output_path=${3:-"$OUTPUT_DIR"}
additional_options=$4

if [ -z "$project_dir" ] || [ -z "$output_path" ]; then
    echo "Error: 'project_dir' or 'output_path' is not set."
    exit 1
fi


# Setup environment variables
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir="$HOME/localConfig"
source "${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" 
asetup StatAnalysis, 0.2.5

# Navigate to the project directory
[ -d "$output_path" ] || mkdir -p "$output_path"
cd "$output_path" || exit

base_name=$(basename "$project_dir")
limit_variant="${base_name#Benchmark}"
echo $limit_variant

if [ ! -d "Results_WZ$limit_variant" ]; then
  ln -s "$project_dir"/Results_WZ$limit_variant Results_WZ$limit_variant
  ln -s "$project_dir"/Results_ssWW$limit_variant Results_ssWW$limit_variant
  ln -s "$project_dir"/Results_MultiFit$limit_variant Results_MultiFit$limit_variant
fi
# Array of config files
config_files=(
  "Results_WZ$limit_variant/Anal_Run2WZ_m${wp_mass}.config"
  "Results_ssWW$limit_variant/Anal_Run2ssWW_m${wp_mass}.config"
)

# Loop through each config file
for file in "${config_files[@]}"; do
    echo "Running trexfitter for file: $file"
    filename=$(basename "$file")
    log_output_path="${output_path}/logs/log_${filename}"
    mkdir -p "$log_output_path"

    # Array of options for trex-fitter
    options=(h w d b f p l s r i)

    # Loop through each option
    for option in "${options[@]}"; do
        # Execute trex-fitter and tee the output to a log file
        trex-fitter "$option" "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_${option}.txt"
    done
done

# Run the combination
file="Results_MultiFit$limit_variant/Anal_Run2Combination_m${wp_mass}.config"
echo "Running trexfitter for file: $file"
filename=$(basename "$file")
log_output_path="${output_path}/logs/log_${filename}"
mkdir -p "$log_output_path"

trex-fitter mwflr "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_mwflr.txt"
# Array of options for trex-fitter
# options=(m w f r)
# for option in "${options[@]}"; do
#     # Execute trex-fitter and tee the output to a log file
#     trex-fitter "$option" "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_${option}.txt"
# done 
