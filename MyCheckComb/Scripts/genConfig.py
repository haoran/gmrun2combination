from pathlib import Path
import argparse
import logging 
logging.basicConfig(level=logging.ERROR)
hist_dir = "/data/hrzhao/Samples/ssWWWZ_run3/inputs_run2_combination/pos_final_v2"

script_path = Path(__file__).resolve()
script_dir = script_path.parent

def copyConfig(input_config_path, output_config_dir):
    input_config_path = Path(input_config_path)
    output_config_dir = Path(output_config_dir)

    if not input_config_path.exists():
        raise ValueError(f"input_config_path: {input_config_path} does not exist")
    if not output_config_dir.exists():
        output_config_dir.mkdir(parents=True, exist_ok=True)

    output_config_path = output_config_dir / input_config_path.name
    logging.info(f"Copying {input_config_path} to {output_config_path}")

    # Read the contents of the config file
    with open(input_config_path, "r") as file:
        config = file.read()

    # Replace the hist_dir line
    config = config.replace('HistoPath: "/eos/atlas/atlascerngroupdisk/phys-hdbs/dbl/ssWWWZ_run3/inputs_run2_combination/pos_final_v2"',
                            f'HistoPath: "{hist_dir}"')
    
    # Write the new config file
    with open(output_config_path, "w") as file:
        file.write(config)
    
    logging.info(f"Done copying {input_config_path} to {output_config_path}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_config_path", type=str, help="Path to the input config file")  
    parser.add_argument("output_config_dir", type=str, help="Path to the output config directory")  
    args = parser.parse_args()

    logging.info(f"input-config-path: {args.input_config_path}")  
    logging.info(f"output-config-dir: {args.output_config_dir}")  
    copyConfig(args.input_config_path, args.output_config_dir)
