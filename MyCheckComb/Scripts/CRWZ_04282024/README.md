# Trexfitter + Condor
## Exclude WZCR in the WZ channel
``` bash
export OUTPUT_DIR=/data/hrzhao/MyOutputs/Run2GMCombination/MyCheckComb/04282024/Benchmark_limitsGM_excludeWZ_WZ/

export excludeWZ_WZ=true
condor_submit submit_comb_benchmark_CRWZ.condor
```

## Exclude WZCR in the ssWW channel
``` bash 
export OUTPUT_DIR=/data/hrzhao/MyOutputs/Run2GMCombination/MyCheckComb/04282024/Benchmark_limitsGM_excludeWZ_ssWW/

export excludeWZ_WZ=false
condor_submit submit_comb_benchmark_CRWZ.condor
```

# Plotting