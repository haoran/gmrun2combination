#!/bin/bash

# Use "$@" to handle all arguments including spaces correctly
wp_mass=$1
project_dir=${2:-"$CONFIG_DIR"}
output_path=${3:-"$OUTPUT_DIR"}
additional_options=$4

# obtain the excludeWZ_WZ variable, if not set, default to false
excludeWZ_WZ="${excludeWZ_WZ:-false}"

# flip the excludeWZ_WZ variable
if [[ "$excludeWZ_WZ" == "true" ]]; then
    excludeWZ_ssWW="false"
elif [[ "$excludeWZ_WZ" == "false" ]]; then
    excludeWZ_ssWW="true"
else
    echo "Error: 'excludeWZ_WZ' is not set to 'true' or 'false'."
    exit 1
fi

echo "excludeWZ_WZ is set to $excludeWZ_WZ"
echo "excludeWZ_ssWW is set to $excludeWZ_ssWW"

if [ -z "$project_dir" ] || [ -z "$output_path" ]; then
    echo "Error: 'project_dir' or 'output_path' is not set."
    exit 1
fi

# Setup environment variables
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir="$HOME/localConfig"
source "${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" 
asetup StatAnalysis, 0.2.5

# Navigate to the project directory
[ -d "$output_path" ] || mkdir -p "$output_path"
cd "$output_path" || exit

base_name=$(basename "$project_dir")
limit_variant="${base_name#Benchmark_}" # e.g. limitsGM or limitsGM_JESJERUncor 

# use awk to parse the limit variant and store it to a list 
readarray -t new_parse < <(echo $limit_variant | awk 'BEGIN{FS="_"} {for(i=1; i<=NF; i++) print $i}')
echo "Parsed array:"
printf "%s\n" "${new_parse[@]}"

array_length=${#new_parse[@]}

# echo $limit_variant

# Array of config files
if [ $array_length -eq 1 ]; then
    config_file_patterns=(
        "${project_dir}/Results_WZ_${limit_variant}/Anal_Run2WZ_m${wp_mass}.config"
        "${project_dir}/Results_ssWW_${limit_variant}/Anal_Run2ssWW_m${wp_mass}.config"
    )
elif [ $array_length -eq 2 ]; then
    # Adjusted index for accessing second element of new_parse
    config_file_patterns=(
        "${project_dir}/Results_WZ_${limit_variant}/Anal_Run2WZ_m${wp_mass}_${new_parse[1]}.config"
        "${project_dir}/Results_ssWW_${limit_variant}/Anal_Run2ssWW_m${wp_mass}_${new_parse[1]}.config"
    )
else
    # Handle error or unexpected case
    echo "Unexpected number of elements in limit_variant"
    # Possible error handling or default case action here
fi

for pattern in "${config_file_patterns[@]}"; do
    # find the pattern in the directory
    dir_path=$(dirname "$pattern")
    file_pattern=$(basename "$pattern")

    # find the files that match the pattern and read the results into an array
    while IFS= read -r -d $'\0' file; do
        config_files+=("$file")
    done < <(find "$dir_path" -type f -name "$file_pattern" -print0)
done

for file in "${config_files[@]}"; do
    echo "Found config file: $file"
done

# Loop through each config file
for file in "${config_files[@]}"; do
    echo "Running trexfitter for file: $file"
    filename=$(basename "$file")
    log_output_path="${output_path}/logs/log_${filename}"
    mkdir -p "$log_output_path"
    additional_options_WZ="$additional_options"

    if [[ "$file" == *"Results_WZ_limitsGM"* ]] && [[ "$excludeWZ_WZ" == "true" ]]; then
        additional_options_WZ="Exclude=CRWZ_WZ:$additional_options_WZ"
    elif [[ "$file" == *"Results_ssWW_limitsGM"* ]] && [[ "$excludeWZ_ssWW" == "true" ]]; then
        additional_options_WZ="Exclude=CRssWW_WZ:$additional_options_WZ"
    fi

    echo "additional_options_WZ: $additional_options_WZ"

    # Array of options for trex-fitter
    options=(h w d f p l s r i)
    # options=(h w f l)

    # Loop through each option
    for option in "${options[@]}"; do
        # Execute trex-fitter and tee the output to a log file
        trex-fitter "$option" "$file" "OutputDir=${output_path}:${additional_options_WZ}" | tee "${log_output_path}/fit_${option}.txt"
    done
done

# ############################################
# # Run the combination
# ############################################
if [ $array_length -eq 1 ]; then
    file="${project_dir}/Results_MultiFit_${limit_variant}/Anal_Run2Combination_m${wp_mass}.config"
elif [ $array_length -eq 2 ]; then
    # Adjusted index for accessing second element of new_parse
    file="${project_dir}/Results_MultiFit_${limit_variant}/Anal_Run2Combination_m${wp_mass}_${new_parse[1]}.config"
else
    # Handle error or unexpected case
    echo "Unexpected number of elements in limit_variant"
    # Possible error handling or default case action here
fi

echo "Running trexfitter for file: $file"
filename=$(basename "$file")
log_output_path="${output_path}/logs/log_${filename}"
mkdir -p "$log_output_path"

# trex-fitter mwflr "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_mwflr.txt"
# Array of options for trex-fitter
options=(mw mf ml mr)
for option in "${options[@]}"; do
    # Execute trex-fitter and tee the output to a log file
    trex-fitter "$option" "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_${option}.txt"
done 
