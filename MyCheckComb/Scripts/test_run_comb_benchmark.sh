#!/bin/bash

# Use "$@" to handle all arguments including spaces correctly
wp_mass=$1
project_dir=${2:-"/home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02052024/Benchmark/"}
output_path=${3:-"/data/hrzhao/MyOutputs/Run2GMCombination/MyCheckComb/02052024/Benchmark/"}
additional_options=$4

# Setup environment variables
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir="$HOME/localConfig"
source "${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" --quiet
asetup StatAnalysis, 0.2.5

# Navigate to the project directory
cd "$output_path" || exit

ln -s "$project_dir"/Results_WZ_systematics Results_WZ_systematics
ln -s "$project_dir"/Results_ssWW_systematics Results_ssWW_systematics
ln -s "$project_dir"/Results_MultiFit_systematics Results_MultiFit_systematics

# Array of config files
config_files=(
  "Results_WZ_systematics/Anal_Run2WZ_m${wp_mass}.config"
  "Results_ssWW_systematics/Anal_Run2ssWW_m${wp_mass}.config"
)

# Loop through each config file
for file in "${config_files[@]}"; do
    echo "Running trexfitter for file: $file"
    filename=$(basename "$file")
    log_output_path="${output_path}/logs/log_${filename}"
    mkdir -p "$log_output_path"

    # Array of options for trex-fitter
    options=(h w f)

    # Loop through each option
    for option in "${options[@]}"; do
        # Execute trex-fitter and tee the output to a log file
        trex-fitter "$option" "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_${option}.txt"
    done
done

# Run the combination
file="Results_MultiFit_systematics/Anal_Run2Combination_m${wp_mass}.config"
echo "Running trexfitter for file: $file"
filename=$(basename "$file")
log_output_path="${output_path}/logs/log_${filename}"
mkdir -p "$log_output_path"

trex-fitter mwfr "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_mwfr.txt"
# Array of options for trex-fitter
# options=(m w f r)
# for option in "${options[@]}"; do
#     # Execute trex-fitter and tee the output to a log file
#     trex-fitter "$option" "$file" "OutputDir=${output_path}:${additional_options}" | tee "${log_output_path}/fit_${option}.txt"
# done 