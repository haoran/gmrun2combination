#!/bin/bash

SOURCE_DIR=${1:-"/home/hrzhao/Projects/ANA-HDBS-2023-19/run2combination/Fitting/configs"}
OUTPUT_DIR=${2:-"/home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/04282024"}

echo "SOURCE_DIR: $SOURCE_DIR"
echo "OUTPUT_DIR: $OUTPUT_DIR"

script_dir="/home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/Scripts/"
# limit_variants=("limitsSTATONLY" "limitsGM" "limitsXSEC")
limit_variants=("limitsGM")
# limit_variants=("limitsGM" "limitsGM_JESJERUncor" "limitsGM_noJESJER")
channel_variants=("WZ" "ssWW" "MultiFit")

for limit_variant in "${limit_variants[@]}"
do
    for channel_variant in "${channel_variants[@]}"
    do
        find ${SOURCE_DIR}/Results_${channel_variant}_${limit_variant}/ -maxdepth 1 -name "Anal_Run2*.config" -print0 | \
        xargs -0 -I {} python3 $script_dir/genConfig.py {} \
        ${OUTPUT_DIR}/Benchmark_${limit_variant}/Results_${channel_variant}_${limit_variant}/
    done

    # replace the path to global path in the multifit config files
    find ${OUTPUT_DIR}/Benchmark_${limit_variant}/Results_MultiFit_${limit_variant} -type f -name "*.config" -print0 | \
    xargs -0 sed -i "s|config/run2combination/Anal_Run2ssWW|${OUTPUT_DIR}/Benchmark_${limit_variant}/Results_ssWW_${limit_variant}/Anal_Run2ssWW|g"
    
    find ${OUTPUT_DIR}/Benchmark_${limit_variant}/Results_MultiFit_${limit_variant} -type f -name "*.config" -print0 | \
    xargs -0 sed -i "s|config/run2combination/Anal_Run2WZ|${OUTPUT_DIR}/Benchmark_${limit_variant}/Results_WZ_${limit_variant}/Anal_Run2WZ|g"
done


