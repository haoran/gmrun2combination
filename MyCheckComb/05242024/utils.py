import numpy as np
import pandas as pd
import hist
import re
from difflib import get_close_matches
from typing import List, Tuple, Optional
from matplotlib import pyplot as plt

def compare_histograms(
        current_histo: hist.Hist, 
        new_histo: hist.Hist, 
        rtol_threshold: float = 1e-4,
        kfactor: float = 1) -> List[np.ndarray]:
    """
    Compare two histograms bin by bin.

    Args:
        current_histo (hist.Hist): The current histogram.
        new_histo (hist.Hist): The new histogram.
        rtol_threshold (float): The relative tolerance threshold.

    Returns:
        List[Tuple[int, float, float, float, float]]: List of bins with significant differences. Each tuple contains:
            (bin_index, current_value, new_value, current_error, new_error)
    """
    differences = []
    current_histo = current_histo * 1.0/kfactor 
    # if current_histo.axes[0].edges.size != new_histo.axes[0].edges.size or not np.allclose(current_histo.axes[0].edges, new_histo.axes[0].edges):
    if current_histo.axes[0].edges.size != new_histo.axes[0].edges.size:
        # skip the check of bin edges because of 5000 and 1200 
        raise ValueError("Histograms do not have the same binning.")
    
    current_values = current_histo.values()
    new_values = new_histo.values()
    current_errors = np.sqrt(current_histo.variances())
    new_errors = np.sqrt(new_histo.variances())
    
    if not np.allclose(current_values, new_values, rtol=rtol_threshold) or not np.allclose(current_errors, new_errors, rtol=rtol_threshold):
        absolute_diff_yield = np.abs(current_values - new_values)
        relative_diff_yield = np.divide(absolute_diff_yield, new_values, out=np.zeros_like(absolute_diff_yield), where=new_values != 0)
        
        absolute_diff_variances = np.abs(current_errors - new_errors)
        relative_diff_variances = np.divide(absolute_diff_variances, new_errors, out=np.zeros_like(absolute_diff_variances), where=new_errors != 0)

        # print(f"Key: {new_histo.name}")
        # print("Absolute differences in yields:", absolute_diff_yield)
        # print("Relative differences in yields:", relative_diff_yield)
        # print("Absolute differences in variances:", absolute_diff_variances)
        # print("Relative differences in variances:", relative_diff_variances)

        differences.append(absolute_diff_yield)
        differences.append(relative_diff_yield)

        differences.append(absolute_diff_variances)
        differences.append(relative_diff_variances)

    return differences




def split_by_sr_cr(string: str) -> Tuple[str, str, str]:
    """
    Split the string by _sr or _cr.
    
    Args:
        string (str): The input string.
        
    Returns:
        Tuple[str, str, str]: The prefix, the _sr/_cr part, and the suffix.
    """
    match = re.search(r"(_sr|_cr)", string)
    if match:
        region = string[:match.end()]
        syst = string[match.end():]
        return region, syst
    return None 


def getDF(current_histo, new_histo):
    bin_index = [f"Bin{i+1}" for i, _ in enumerate(current_histo.to_numpy()[0])]
    df = pd.DataFrame({
        "current_BinContent" : current_histo.values(),
        "new_BinContent" : new_histo.values(),
        "current_BinError": np.sqrt(current_histo.variances()), 
        "new_BinError" : np.sqrt(new_histo.variances())
    },
    index = bin_index)

    return df


def plot_ratio(two_hists, suptitle="Ratio"):
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [3, 1], 'hspace': 0})
    bin_centers = (two_hists[0].axes[0].edges[:-1] + two_hists[0].axes[0].edges[1:]) / 2

    # Plot the first histogram with error bars
    bin_contents_0 = two_hists[0].values()
    bin_errors_0 = np.sqrt(two_hists[0].variances())
    ax1.errorbar(bin_centers, bin_contents_0, yerr=bin_errors_0, fmt='o', label="Current input", linestyle='None')

    # Plot the second histogram with error bars
    bin_contents_1 = two_hists[1].values()
    bin_errors_1 = np.sqrt(two_hists[1].variances())
    ax1.errorbar(bin_centers, bin_contents_1, yerr=bin_errors_1, fmt='o', label="New input", linestyle='None')

    ax1.legend()
    ax1.set_ylabel("Yields")

    # Plot the ratio
    ratio = np.divide(bin_contents_1, bin_contents_0, out=np.zeros_like(bin_contents_0), where=bin_contents_0 != 0)
    ratio_errors = np.sqrt((bin_errors_1 / bin_contents_0) ** 2 + (bin_contents_1 * bin_errors_0 / bin_contents_0 ** 2) ** 2)
    ax2.errorbar(bin_centers, ratio, yerr=ratio_errors, fmt='o', linestyle='None', label="New/Current")
    ratio_errors[np.isnan(ratio_errors)] = 0  # remove possible NaN 
    ratio[np.isnan(ratio)] = 0  # remove possible NaN 
    ax2.axhline(1, color='gray', linestyle='--')
    ax2.set_ylabel("Ratio")
    ax2.set_xlabel("Bin Center")
    ax2.legend()

    ax2.set_ylim(0.9, 1.1)

    fig.suptitle(suptitle)

    return fig, ax1, ax2