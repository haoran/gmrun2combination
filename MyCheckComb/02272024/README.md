``` bash

./Scripts/gen_config.sh /home/hrzhao/Projects/ANA-HDBS-2023-19/run2combination/Fitting/configs/ /home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02272024

export CONFIG_DIR=/home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02272024/Benchmark_limitsGM_JESJERUncor
export OUTPUT_DIR=/data/hrzhao/MyOutputs/Run2GMCombination/MyCheckComb/02272024/Benchmark_limitsGM_JESJERUncor

condor_submit submit_comb_benchmark_variants.condor

export CONFIG_DIR=/home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02272024/Benchmark_limitsGM
export OUTPUT_DIR=/data/hrzhao/MyOutputs/Run2GMCombination/MyCheckComb/02272024/Benchmark_limitsGM

condor_submit submit_comb_benchmark_variants.condor


export CONFIG_DIR=/home/hrzhao/Projects/ANA-HDBS-2023-19/MyCheckComb/02272024/Benchmark_limitsGM_noJESJER
export OUTPUT_DIR=/data/hrzhao/MyOutputs/Run2GMCombination/MyCheckComb/02272024/Benchmark_limitsGM_noJESJER

condor_submit submit_comb_benchmark_variants.condor


```